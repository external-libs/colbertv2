import setuptools

with open('README.md', 'r') as f:
    long_description = f.read()

setuptools.setup(
    name='ColBERT',
    version='0.2.1',
    author='Omar Khattab',
    author_email='okhattab@stanford.edu',
    description="Efficient and Effective Passage Search via Contextualized Late Interaction over BERT",
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/external-libs/colbertv2',
    packages=setuptools.find_packages(),
    python_requires='>=3.8',
    install_requires=[
        'torch',
        'transformers',
        'faiss-cpu',
        'ujson',
        'GitPython'
        ],
    package_data={'': ['*.cpp']},
    include_package_data=True,
)
